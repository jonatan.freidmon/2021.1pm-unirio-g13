package echo;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import echo.models.Totem;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class TotemControllerTest {
	@Test
    void getAllBicicletas() {
        HttpResponse<Totem[]> bicicletasResponse = Unirest.get("https://my-web-app-pm.herokuapp.com/totem").asObject(Totem[].class);
        assertEquals(200, bicicletasResponse.getStatus());
    }
	
    
    @Test
    void createTotem() {
    	String jsonInputString = "{\"localizacao\": \"Rua Cabo Reis, RJ\"}";  	
        HttpResponse<Totem> totemResponse = Unirest.post("https://my-web-app-pm.herokuapp.com/totem").body(jsonInputString).asObject(Totem.class);
        assertEquals(200, totemResponse.getStatus());
    }
    
    @Test
    void createTotemDadosInvalidos() {
    	String jsonInputString = "{\"localizacao\": \"\"}";  	
        HttpResponse<Totem> totemResponse = Unirest.post("https://my-web-app-pm.herokuapp.com/totem").body(jsonInputString).asObject(Totem.class);
        assertEquals(422, totemResponse.getStatus());
    }
    
    
    @Test
    void updateTotem() {
    	String jsonInputString = "{\"localizacao\": \"Rua Cabo Reis, Ramos, RJ\"}";
        HttpResponse<Totem> totemResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/totem/{id}").routeParam("id", "teste").body(jsonInputString).asObject(Totem.class);
        assertEquals(200, totemResponse.getStatus());
    }
    
    @Test
    void updateTotemDadosInvalidos() {
    	String jsonInputString = "{\"localizacao\": \"\"}";   	
        HttpResponse<Totem> totemResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/totem/{id}").routeParam("id", "teste").body(jsonInputString).asObject(Totem.class);
        assertEquals(422, totemResponse.getStatus());
    }
    
    @Test
    void updateTotemNotFound() {
    	String jsonInputString = "{\"localizacao\": \"Rua Cabo Reis, Ramos, RJ\"}";
        HttpResponse<Totem> totemResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/totem/{id}").routeParam("id", "sdasdasdsadasd").body(jsonInputString).asObject(Totem.class);
        assertEquals(404, totemResponse.getStatus());
    }
    
    @Test
    void deleteTotem() {
        HttpResponse response = Unirest.delete("https://my-web-app-pm.herokuapp.com/totem/{id}").routeParam("id", "totemDelete").asEmpty();
        assertEquals(200, response.getStatus());
        
    }
    
    @Test
    void deleteTotemNotFound() {
    	HttpResponse response = Unirest.delete("https://my-web-app-pm.herokuapp.com/totem/{id}").routeParam("id", "5dasdasdqe1").asEmpty();
    	assertEquals(404, response.getStatus());
    }
    
    @Test
    void deleteTotemDadosInvalidos() {
        HttpResponse response = Unirest.put("https://my-web-app-pm.herokuapp.com/totem/{id}").routeParam("id", "totemRetirarDaRede").asEmpty();
        assertEquals(400, response.getStatus());
    }
}
