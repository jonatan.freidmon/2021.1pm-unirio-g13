package echo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import echo.models.Bicicleta;
import echo.models.StatusBicicleta;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

class BicicletaControllerTest {

    @Test
    void getAllBicicletas() {
        HttpResponse<Bicicleta[]> bicicletasResponse = Unirest.get("https://my-web-app-pm.herokuapp.com/bicicleta").asObject(Bicicleta[].class);
        assertEquals(200, bicicletasResponse.getStatus());
    }
    
    @Test
    void getByIDBicicleta() {
    	Bicicleta bicicletaTeste = new Bicicleta("teste", 1,"testeMarca", 
                "testeModelo", 
                "2014",
                StatusBicicleta.NOVA);
        HttpResponse<Bicicleta> bicicletaResponse = Unirest.get("https://my-web-app-pm.herokuapp.com/bicicleta/{id}").routeParam("id", "teste").asObject(Bicicleta.class);
        Bicicleta getBicicleta = bicicletaResponse.getBody();
        assertEquals(200, bicicletaResponse.getStatus());
        assertEquals(bicicletaTeste.getMarca(), getBicicleta.getMarca());
        assertEquals(bicicletaTeste.getModelo(), getBicicleta.getModelo());
        assertEquals(bicicletaTeste.getAno(), getBicicleta.getAno());
        assertEquals(bicicletaTeste.getStatus(), getBicicleta.getStatus());
    }
    
    @Test
    void getByIDBicicletaNotFound() {
        HttpResponse<Bicicleta> bicicletaResponse = Unirest.get("https://my-web-app-pm.herokuapp.com/bicicleta/{id}").routeParam("id", "3782739e-652b-4a68-9b4f-59a3499bdasdsad").asObject(Bicicleta.class);
        assertEquals(404, bicicletaResponse.getStatus());
    }
    
    @Test
    void createBicicleta() {
    	String jsonInputString = "{\"marca\": \"Gol\", \"modelo\": \"gol\", \"ano\":\"2021\", \"status\":\"NOVA\"}";  	
        HttpResponse<Bicicleta> bicicletaResponse = Unirest.post("https://my-web-app-pm.herokuapp.com/bicicleta").body(jsonInputString).asObject(Bicicleta.class);
        Bicicleta getBicicleta = bicicletaResponse.getBody();
        assertEquals(200, bicicletaResponse.getStatus());
    }
    
    @Test
    void createBicicletaDadosInvalidos() {
    	String jsonInputString = "{\"marca\": true, \"modelo\": true, \"ano\": true, \"status\": DKMASKDMAKAM}";  	
        HttpResponse<Bicicleta> bicicletaResponse = Unirest.post("https://my-web-app-pm.herokuapp.com/bicicleta").body(jsonInputString).asObject(Bicicleta.class);
        assertEquals(400, bicicletaResponse.getStatus());
    }
    
    @Test
    void updateBicicletaNotFound() {
    	String jsonInputString = "{\"marca\": \"Gol\", \"modelo\": \"gol\", \"ano\":\"2021\", \"status\":\"NOVA\"}";
    	Bicicleta bicicleta1 = new Bicicleta("Gol", "gol", "2021", StatusBicicleta.NOVA);
        HttpResponse<Bicicleta> bicicletaResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/bicicleta/{id}").routeParam("id", "3782739e-652b-4a68-9b4f-dasdkmsakdmask").body(jsonInputString).asObject(Bicicleta.class);
        assertEquals(404, bicicletaResponse.getStatus());
    }
    
    @Test
    void updateBicicletaDadosInvalidos() {
    	String jsonInputString = "{\"marca\": true, \"modelo\": true, \"ano\": true, \"status\": DKMASKDMAKAM}";
        HttpResponse<Bicicleta> bicicletaResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/bicicleta/{id}").routeParam("id", "3782739e-652b-4a68-9b4f-dasdkmsakdmask").body(jsonInputString).asObject(Bicicleta.class);
        assertEquals(400, bicicletaResponse.getStatus());
    }
    
    @Test
    void deleteBicicleta() {
        HttpResponse response = Unirest.delete("https://my-web-app-pm.herokuapp.com/bicicleta/{id}").routeParam("id", "bicicletaDelete").asEmpty();
        assertEquals(200, response.getStatus());
        
    }
    
    @Test
    void deleteBicicletaNotFound() {
    	HttpResponse response = Unirest.delete("https://my-web-app-pm.herokuapp.com/bicicleta/{id}").routeParam("id", "3782739e-652b-4a68-9b4f-dasdasdasddaaaa").asEmpty();
    	assertEquals(404, response.getStatus());
    }
    
    @Test
    void deleteBicicletaDadosInvalidos() {
        HttpResponse response = Unirest.put("https://my-web-app-pm.herokuapp.com/bicicleta/{id}").routeParam("id", "3782739e-652b-4a68-9b4f-59a3499bfba6").asEmpty();
        assertEquals(400, response.getStatus());
    }
    
    @Test
    void inserirNaRede() {
    	String jsonInputString = "{\"idBicicleta\": \"3782739e-652b-4a68-9b4f-59a3499bfba6\", \"idTranca\": \"5ea95fe2-18d9-4e0c-a073-08cb30e3e4e4\"}";
    	HttpResponse response = Unirest.post("https://my-web-app-pm.herokuapp.com/bicicleta/integrarNaRede").body(jsonInputString).asEmpty();
    	assertEquals(200, response.getStatus());
    }
    
    @Test
    void retirarDaRede() {
    	String jsonInputString = "{\"idBicicleta\": \"testeRetirarDaRede\", \"idTranca\": \"trancaRetirarDaRede\"}";
    	HttpResponse response = Unirest.post("https://my-web-app-pm.herokuapp.com/bicicleta/retirarDaRede").body(jsonInputString).asEmpty();
    	assertEquals(200, response.getStatus());
    }   
}