package echo;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import echo.models.StatusTranca;
import echo.models.Tranca;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class TrancaControllerTest {

	@Test
    void getAllTrancas() {
        HttpResponse<Tranca[]> trancasResponse = Unirest.get("https://my-web-app-pm.herokuapp.com/tranca").asObject(Tranca[].class);
        assertEquals(200, trancasResponse.getStatus());
    }
    
    @Test
    void getByIDTranca() {
    	Tranca tranca = new Tranca("5ea95fe2-18d9-4e0c-a073-08cb30easdasdsa", "Rua Uranos, ramos, rj", 
                "tranca 2160", 
                "2017",
                StatusTranca.NOVA);
        HttpResponse<Tranca> trancaResponse = Unirest.get("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "5ea95fe2-18d9-4e0c-a073-08cb30easdasdsa").asObject(Tranca.class);
        Tranca getTranca = trancaResponse.getBody();
        assertEquals(200, trancaResponse.getStatus());
        assertEquals(tranca.getLocalizacao(), getTranca.getLocalizacao());
        assertEquals(tranca.getModelo(), getTranca.getModelo());
        assertEquals(tranca.getAnoDeFabricacao(), getTranca.getAnoDeFabricacao());
        assertEquals(tranca.getStatus(), getTranca.getStatus());
    }
    
    @Test
    void getByIDTrancaNotFound() {
    	HttpResponse<Tranca> trancaResponse = Unirest.get("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "5ea95fe2-18d9-4e0c-a073-dasdas").asObject(Tranca.class);
        assertEquals(404, trancaResponse.getStatus());
    }
    
    @Test
    void createTranca() {
    	String jsonInputString = "{\"localizacao\": \"Rua Cabo Reis, RJ\", \"modelo\": \"gol\", \"anoDeFabricacao\":\"2021\", \"status\":\"LIVRE\"}";  	
        HttpResponse<Tranca> TrancaResponse = Unirest.post("https://my-web-app-pm.herokuapp.com/tranca").body(jsonInputString).asObject(Tranca.class);
        assertEquals(200, TrancaResponse.getStatus());
    }
    
    @Test
    void createTrancaDadosInvalidos() {
    	String jsonInputString = "{\"localizacao\": true, \"modelo\": true, \"anoDeFabricacao\": true, \"status\":\"DASDSADASD\"}";  	
        HttpResponse<Tranca> trancaResponse = Unirest.post("https://my-web-app-pm.herokuapp.com/tranca").body(jsonInputString).asObject(Tranca.class);
        assertEquals(400, trancaResponse.getStatus());
    }
    
    
    @Test
    void updateTranca() {
    	String jsonInputString = "{\"localizacao\": \"Rua Cabo Reis, RJ\", \"modelo\": \"gol\", \"anoDeFabricacao\":\"2021\", \"status\":\"LIVRE\"}";
        HttpResponse<Tranca> trancaResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "5ea95fe2-18d9-4e0c-a073-08cb30e3e4e4").body(jsonInputString).asObject(Tranca.class);
        assertEquals(200, trancaResponse.getStatus());
    }
    
    @Test
    void updateTrancaDadosInvalidos() {
    	String jsonInputString = "{\"localizacao\": true, \"modelo\": true, \"anoDeFabricacao\": true, \"status\":\"DASDSADASD\"}";  	
        HttpResponse<Tranca> trancaResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "5ea95fe2-18d9-4e0c-a073-08cb30e3e4e4").body(jsonInputString).asObject(Tranca.class);
        assertEquals(400, trancaResponse.getStatus());
    }
    
    @Test
    void updateTrancaNotFound() {
    	String jsonInputString = "{\"localizacao\": \"Rua Cabo Reis, RJ\", \"modelo\": \"gol\", \"anoDeFabricacao\":\"2021\", \"status\":\"LIVRE\"}";
        HttpResponse<Tranca> trancaResponse = Unirest.put("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "3782739e-652b-4a68-9b4f-dasdkmsakdmask").body(jsonInputString).asObject(Tranca.class);
        assertEquals(404, trancaResponse.getStatus());
    }
    
    @Test
    void deleteBicicleta() {
        HttpResponse response = Unirest.delete("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "trancaDelete").asEmpty();
        assertEquals(200, response.getStatus());
        
    }
    
    @Test
    void deleteBicicletaNotFound() {
    	HttpResponse response = Unirest.delete("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "5ea95fe2-18d9-4e0c-a073-dasdasasw1").asEmpty();
    	assertEquals(404, response.getStatus());
    }
    
    @Test
    void deleteBicicletaDadosInvalidos() {
        HttpResponse response = Unirest.put("https://my-web-app-pm.herokuapp.com/tranca/{id}").routeParam("id", "testeRetirarDaRede").asEmpty();
        assertEquals(400, response.getStatus());
    }
    
    @Test
    void inserirNaRede() {
    	String jsonInputString = "{\"idTotem\": \"teste\", \"idTranca\": \"teste\"}";
    	HttpResponse response = Unirest.post("https://my-web-app-pm.herokuapp.com/tranca/integrarNaRede").body(jsonInputString).asEmpty();
    	assertEquals(200, response.getStatus());
    }
    
    @Test
    void retirarDaRede() {
    	String jsonInputString = "{\"idTotem\": \"totemRetirarDaRede\", \"idTranca\": \"retirarDaRede\"}";
    	HttpResponse response = Unirest.post("https://my-web-app-pm.herokuapp.com/tranca/retirarDaRede").body(jsonInputString).asEmpty();
    	assertEquals(200, response.getStatus());
    }   
	
}
