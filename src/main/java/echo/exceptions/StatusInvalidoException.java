package echo.exceptions;

public class StatusInvalidoException  extends Exception {
	public StatusInvalidoException(String mensagem) {
		super(mensagem);
	}
}
