package echo.exceptions;

public class NaoEncontradoException extends Exception {
	public NaoEncontradoException(String mensagem) {
		super(mensagem);
	}

}
