package echo.models;

import java.util.UUID;

public class Funcionario {
	
	private String matricula;
    private String senha;
    private String email;
    private String nome;
    private int idade;
    private String funcao;
    private String cpf;
    
    public String getMatricula(){
        return this.matricula;
    }

    public String getSenha(){
        return this.senha;
    }

    public String getEmail(){
        return this.email;
    }

    public String getNome(){
        return this.nome;
    }

    public int getIdade(){
        return this.idade;
    }

    public String getFuncao(){
        return this.funcao;
    }

    public String getCpf(){
        return this.cpf;
    }

    public void setMatricula(String matricula){
        this.matricula = matricula;
    }

    public void setSenha(String senha){
        this.senha = senha;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public void setIdade(int idade){
        this.idade = idade;
    }
    
    public void setFuncao(String funcao){
        this.funcao = funcao;
    }

    public void setCpf(String cpf){
        this.cpf = cpf;
    }
	
}
