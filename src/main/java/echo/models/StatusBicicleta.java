package echo.models;

public enum StatusBicicleta {
	DISPONIVEL, EM_USO, NOVA, APOSENTADA, REPARO_SOLICITADO, EM_REPARO
}
