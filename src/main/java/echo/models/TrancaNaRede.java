package echo.models;

public class TrancaNaRede {
    
    private String idTotem;
    private String idTranca;
    private String status;

    public TrancaNaRede(){

    }

    public TrancaNaRede(String idTotem, String idTranca, String status){
        this.idTotem = idTotem;
        this.idTranca = idTranca;
        this.status = status;
    }

    public String getIdTotem(){
        return this.idTotem;
    }

    public String getIdTranca(){
        return this.idTranca;
    }

    public String getStatus(){
        return this.status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public void setIdTotem(String idTotem){
        this.idTotem = idTotem;
    }

    public void setIdTranca(String idTranca){
        this.idTranca = idTranca;
    }


}