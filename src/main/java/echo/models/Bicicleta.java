package echo.models;

import java.util.UUID;
import java.lang.Math;

public class Bicicleta {
    
    private String id;
    private String marca;
    private String modelo;
    private String ano;
    private int numero;
    private StatusBicicleta status;
    private String idTranca;

    public Bicicleta(){
    }
    
    public Bicicleta(String id, int numero, String marca, String modelo, String ano, StatusBicicleta status){
        this.id = id;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.numero = numero;
        this.status = status;
        this.idTranca = "";
    }
    
    public Bicicleta(String id, String marca, String modelo, String ano, StatusBicicleta status){
        this.id = id;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.numero = (int) ((Math.random() * (1000000 - 1)) + 1);
        this.status = status;
        this.idTranca = "";
    }

    public Bicicleta(String marca, String modelo, String ano, StatusBicicleta status){
        this.id = UUID.randomUUID().toString();
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.numero = (int) ((Math.random() * (1000000 - 1)) + 1);
        this.status = status;
        this.idTranca = "";
    }

    public String getId(){
        return this.id;
    }

    public String getMarca(){
        return this.marca;
    }

    public String getModelo(){
        return this.modelo;
    }

    public String getAno(){
        return this.ano;
    }

    public int getNumero(){
        return this.numero;
    }

    public StatusBicicleta getStatus(){
        return this.status;
    }

    public String getTranca(){
        return this.idTranca;
    }

    public void setMarca(String marca){
        this.marca = marca;
    }

    public void setModelo(String modelo){
        this.modelo = modelo;
    }

    public void setAno(String ano){
        this.ano = ano;
    }

    public void setStatus(StatusBicicleta status){
        this.status = status;
    }

    public void setTranca(String idTranca){
        this.idTranca = idTranca;
    }

}