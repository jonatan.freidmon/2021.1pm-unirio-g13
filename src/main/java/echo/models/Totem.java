package echo.models;

import java.util.UUID;
import java.util.ArrayList;

public class Totem {
    
    private String id;
    private String localizacao;
    private ArrayList<Tranca> trancas;
    
    public Totem(){

    }

    public Totem(String id, String localizacao){
        this.id = id;
        this.localizacao = localizacao;
        this.trancas = new ArrayList<Tranca>();
    }
    
    public Totem(String localizacao){
        this.id = UUID.randomUUID().toString();
        this.localizacao = localizacao;
        this.trancas = new ArrayList<Tranca>();
    }
    

    public String getId(){
        return this.id;
    }

    public String getLocalizacao(){
        return this.localizacao;
    }

    public ArrayList<Tranca> getTrancas(){
        return this.trancas;
    }

    public void setLocalizacao(String localizacao){
        this.localizacao = localizacao;
    }

    public void setTrancas(ArrayList<Tranca> trancas){
        this.trancas = trancas;
    }

    public void addTranca(Tranca tranca){
        this.trancas.add(tranca);
    }

    public void removeTranca(Tranca tranca){
        this.trancas.remove(tranca);
    }
}