package echo.models;

import java.util.UUID;
import java.lang.Math;

public class Tranca {
    
    private String id;
    private String localizacao;
    private String modelo;
    private String anoDeFabricacao;
    private int numero;
    private StatusTranca status;
    private Bicicleta bicicleta;
    
    public Tranca(){

    }

    public Tranca(String id, String localizacao, String modelo, String anoDeFabricacao, StatusTranca status){
        this.id = id;
        this.localizacao = localizacao;
        this.modelo = modelo;
        this.anoDeFabricacao = anoDeFabricacao;
        this.numero = (int) ((Math.random() * (1000000 - 1)) + 1);
        this.status = status;
        this.bicicleta = null;
    }
    
    public Tranca(String localizacao, String modelo, String anoDeFabricacao, StatusTranca status){
        this.id = UUID.randomUUID().toString();
        this.localizacao = localizacao;
        this.modelo = modelo;
        this.anoDeFabricacao = anoDeFabricacao;
        this.numero = (int) ((Math.random() * (1000000 - 1)) + 1);
        this.status = status;
        this.bicicleta = null;
    }

    public String getId(){
        return this.id;
    }

    public String getLocalizacao(){
        return this.localizacao;
    }

    public String getModelo(){
        return this.modelo;
    }

    public String getAnoDeFabricacao(){
        return this.anoDeFabricacao;
    }

    public int getNumero(){
        return this.numero;
    }

    public StatusTranca getStatus(){
        return this.status;
    }

    public Bicicleta getBicicleta(){
        return this.bicicleta;
    }

    public void setLocalizacao(String localizacao){
        this.localizacao = localizacao;
    }

    public void setModelo(String modelo){
        this.modelo = modelo;
    }

    public void setAnoDeFabricacao(String anoDeFabricacao){
        this.anoDeFabricacao = anoDeFabricacao;
    }

    public void setStatus(StatusTranca status){
        this.status = status;
    }

    public void setBicicleta(Bicicleta bicicleta){
        this.bicicleta = bicicleta;
    }

}