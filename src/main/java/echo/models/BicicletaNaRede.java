package echo.models;

public class BicicletaNaRede {
    
    private String idBicicleta;
    private String idTranca;
    private String status;

    public BicicletaNaRede(){

    }

    public BicicletaNaRede(String idBicicleta, String idTranca, String status){
        this.idBicicleta = idBicicleta;
        this.idTranca = idTranca;
        this.status = status;
    }

    public String getIdBicicleta(){
        return this.idBicicleta;
    }

    public String getIdTranca(){
        return this.idTranca;
    }

    public String getStatus(){
        return this.status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public void setIdBicicleta(String idBicicleta){
        this.idBicicleta = idBicicleta;
    }

    public void setIdTranca(String idTranca){
        this.idTranca = idTranca;
    }


}