package echo.models;

public enum StatusTranca {
	LIVRE, OCUPADA, APOSENTADA, EM_REPARO, NOVA, REPARO_SOLICITADO
}
