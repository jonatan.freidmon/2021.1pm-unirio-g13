package echo.services;

import java.util.ArrayList;
import echo.exceptions.DadosInvalidosException;
import echo.exceptions.NaoEncontradoException;
import echo.models.Tranca;
import echo.models.StatusTranca;
import echo.models.Bicicleta;
import echo.models.StatusBicicleta;
import echo.models.TrancaNaRede;
import echo.models.Totem;

public class TrancaService {

    final String mensagemDadosInvalidos = "Dados invalidos.";
    final String mensagemTrancaNaoEncontrada = "Tranca nao foi encontrada.";
    final String APOSENTADA = "APOSENTADA";
    final String EM_REPARO = "EM_REPARO";
    public ArrayList<Tranca> trancas;

    public TrancaService(){
        this.trancas = new ArrayList<Tranca>();
        Tranca tranca1 = new Tranca("5ea95fe2-18d9-4e0c-a073-08cb30e3e4e4", "Rua Teste, rj", 
                                            "tranca 2159", 
                                            "2018",
                                            StatusTranca.LIVRE);
        Tranca tranca2 = new Tranca("5ea95fe2-18d9-4e0c-a073-08cb30easdasdsa", "Rua Uranos, ramos, rj", 
                "tranca 2160", 
                "2017",
                StatusTranca.NOVA);
        Tranca trancaDelete = new Tranca("trancaDelete", "Rua Professor Lace, ramos, rj", 
                "tranca 2161", 
                "2015",
                StatusTranca.NOVA);
        Tranca tranca4 = new Tranca("teste", "Rua Cabo Reis 21, ramos, rj", 
                "tranca 2162", 
                "2013",
                StatusTranca.NOVA);
        Tranca retirarDaRede = new Tranca("retirarDaRede", "Piscinao, ramos, rj", 
                "tranca 2163", 
                "2012",
                StatusTranca.REPARO_SOLICITADO);
        Tranca trancaRetirarDaRede = new Tranca("trancaRetirarDaRede", "trancaRetirarDaRedeMarca", 
                "trancaRetirarDaRedeModelo", 
                "2010",
                StatusTranca.OCUPADA);
        Bicicleta bicicletaRetirarDaRede = new Bicicleta("testeRetirarDaRede", 2, "testeRetirarDaRedeMarca", "testeRetirarDaRedeModelo", "2021", StatusBicicleta.REPARO_SOLICITADO);
        trancaRetirarDaRede.setBicicleta(bicicletaRetirarDaRede);
        this.trancas.add(tranca1);
        this.trancas.add(tranca2);
        this.trancas.add(trancaRetirarDaRede);
        this.trancas.add(retirarDaRede);
        this.trancas.add(tranca4);
        this.trancas.add(trancaDelete);
    }

    public ArrayList<Tranca> getAll(){
 
        return this.trancas;
    }

    public Tranca getById(String id) throws NaoEncontradoException {
        Tranca trancaRecuperada = null;
        for(Tranca tranca : this.trancas){
            if(tranca.getId().equals(id)){
                trancaRecuperada = tranca;
                break;
            }
        }

        if(trancaRecuperada != null){
            return trancaRecuperada;
        }
        else{
            throw new NaoEncontradoException(this.mensagemTrancaNaoEncontrada);
        }

    }

    public Tranca add(Tranca tranca) throws DadosInvalidosException{
        if(verificarCamposObrigatorios(tranca)){
            Tranca novaTranca = new Tranca(tranca.getLocalizacao(), 
                                              tranca.getModelo(), 
                                              tranca.getAnoDeFabricacao(),
                                              StatusTranca.NOVA);
            this.trancas.add(novaTranca);
            return novaTranca;
        }
        else{
            throw new DadosInvalidosException(this.mensagemDadosInvalidos);
        }
    }

    public Tranca update(String id, Tranca tranca) throws DadosInvalidosException, NaoEncontradoException{
        try{
            Tranca trancaRecuperada = this.getById(id);
            if(verificarCamposObrigatorios(tranca)){
                int index = this.retornaIndexTrancaNaLista(id);
                trancaRecuperada.setModelo(tranca.getModelo());
                trancaRecuperada.setAnoDeFabricacao(tranca.getAnoDeFabricacao());
                trancaRecuperada.setLocalizacao(tranca.getLocalizacao());
                if(tranca.getBicicleta() != null){
                    trancaRecuperada.setBicicleta(tranca.getBicicleta());
                }
                this.trancas.set(index, trancaRecuperada);
                return trancaRecuperada;
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTrancaNaoEncontrada);
        }
    }

    public void delete(String id) throws NaoEncontradoException, DadosInvalidosException{
        try{
            Tranca trancaRecuperada = this.getById(id);
            if(this.validarExclusaoTranca(trancaRecuperada)){
                this.trancas.remove(trancaRecuperada);
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
            
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTrancaNaoEncontrada);
        }
    }

    public Bicicleta getBicicletaByTranca(String id) throws NaoEncontradoException{
        try{
            Tranca trancaRecuperada = this.getById(id);
            return trancaRecuperada.getBicicleta();

        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTrancaNaoEncontrada);
        }

    }

    public Tranca changeStatus(String id, String acao) throws DadosInvalidosException, NaoEncontradoException{
        try{
            Tranca trancaRecuperada = this.getById(id);
            int index = this.retornaIndexTrancaNaLista(id);
            switch (acao) {
                case "NOVA":
                    trancaRecuperada.setStatus(StatusTranca.NOVA);
                    break;
                case "LIVRE":
                    trancaRecuperada.setStatus(StatusTranca.LIVRE);
                    break;
                case "OCUPADA":
                    trancaRecuperada.setStatus(StatusTranca.OCUPADA);
                    break;
                case "APOSENTADA":
                    trancaRecuperada.setStatus(StatusTranca.APOSENTADA);
                    break;
                case "EM_REPARO":
                    trancaRecuperada.setStatus(StatusTranca.EM_REPARO);
                    break;
                case "REPARO_SOLICITADO":
                    trancaRecuperada.setStatus(StatusTranca.REPARO_SOLICITADO);
                    break;           
                default:
                    throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
            this.trancas.set(index, trancaRecuperada);
            return trancaRecuperada;
                

        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTrancaNaoEncontrada);
        }
    }
    
    public void inserirNaRede(TrancaNaRede trancaNaRede, TotemService totemService) throws NaoEncontradoException, DadosInvalidosException{
        try{
            Tranca tranca = this.getById(trancaNaRede.getIdTranca());
            Totem totem = totemService.getById(trancaNaRede.getIdTotem());
            if(this.validarInserirTrancaNaRede(tranca)){
                totem.addTranca(tranca);
                totemService.update(totem.getId(), totem);
                this.changeStatus(tranca.getId(), "LIVRE");
                this.update(tranca.getId(), tranca);
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }

        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTrancaNaoEncontrada);
        }
    }

    public void retirarDaRede(TrancaNaRede trancaNaRede, TotemService totemService) throws NaoEncontradoException, DadosInvalidosException{
        try{
            Tranca tranca = this.getById(trancaNaRede.getIdTranca());
            Totem totem = totemService.getById(trancaNaRede.getIdTotem());
            if(this.validarRetirarTrancaDaRede(tranca)){
                if(trancaNaRede.getStatus().equals(APOSENTADA))
                    this.changeStatus(tranca.getId(), APOSENTADA);
                else if(trancaNaRede.getStatus().equals(EM_REPARO))    
                    this.changeStatus(tranca.getId(), EM_REPARO);
                else
                    throw new DadosInvalidosException(this.mensagemDadosInvalidos);   
                totem.removeTranca(tranca);
                totemService.update(totem.getId(), totem);     
                this.update(tranca.getId(), tranca);
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }

            //EMAIL ENVIADO PARA O REPARADOR
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTrancaNaoEncontrada);
        }
    }

    private boolean verificarCamposObrigatorios(Tranca tranca){
        if(!tranca.getModelo().isEmpty() && !tranca.getAnoDeFabricacao().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    
    private int retornaIndexTrancaNaLista(String id){
        int indexTranca = -1;
        for(Tranca tranca : this.trancas){
            indexTranca++;
            if(tranca.getId().equals(id)){
                break;
            }
        }
        return indexTranca;
    }

    private boolean validarExclusaoTranca(Tranca tranca){
        if(tranca.getBicicleta() == null){
            return true;
        }
        else{
            return false;
        }
    }

    private boolean validarInserirTrancaNaRede(Tranca tranca){
        if(tranca.getStatus().equals(StatusTranca.NOVA) || tranca.getStatus().equals(StatusTranca.EM_REPARO)){
            return true;
        }
        else{
            return false;
        }
    }
    private boolean validarRetirarTrancaDaRede(Tranca tranca){
        if(tranca.getStatus().equals(StatusTranca.REPARO_SOLICITADO) && tranca.getBicicleta() == null){
            return true;
        }
        else{
            return false;
        }
    }
}