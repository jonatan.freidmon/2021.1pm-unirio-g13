package echo.services;

import java.util.ArrayList;
import echo.exceptions.DadosInvalidosException;
import echo.exceptions.NaoEncontradoException;
import echo.exceptions.StatusInvalidoException;
import echo.models.Bicicleta;
import echo.models.StatusBicicleta;
import echo.models.StatusTranca;
import echo.models.Tranca;
import echo.models.BicicletaNaRede;
import echo.services.TrancaService;

public class BicicletaService {

    final String mensagemDadosInvalidos = "Dados invalidos.";
    final String mensagemBicicletaNaoEncontrada = "Tranca nao foi encontrada.";
    final String APOSENTADA = "APOSENTADA";
    final String EM_REPARO = "EM_REPARO";
    
    public ArrayList<Bicicleta> bicicletas;

    public BicicletaService(){
        this.bicicletas = new ArrayList<Bicicleta>();
        Bicicleta bicicletaTeste = new Bicicleta("teste", 1,"testeMarca", 
                "testeModelo", 
                "2014",
                StatusBicicleta.NOVA);
        Bicicleta bicicleta1 = new Bicicleta("3782739e-652b-4a68-9b4f-59a3499bfba6","Caloi 2020", 
                                            "Caloi", 
                                            "2020",
                                            StatusBicicleta.NOVA);
        Bicicleta bicicleta2 = new Bicicleta("3782739e-652b-4a68-9b4f-59a34992121","KALOY", 
                "KALOY", 
                "2010",
                StatusBicicleta.DISPONIVEL);
        Bicicleta bicicleta3 = new Bicicleta("3782739e-652b-4a68-9b4f-59a3dasdd","QUELOI", 
                "QUELOI", 
                "2019",
                StatusBicicleta.APOSENTADA);
        Bicicleta bicicletaDelete = new Bicicleta("bicicletaDelete","CA2021LOI", 
                "CA2021LOI", 
                "2021",
                StatusBicicleta.APOSENTADA);
        Bicicleta bicicletaRetirarDaRede = new Bicicleta("testeRetirarDaRede", 2, "testeRetirarDaRedeMarca", "testeRetirarDaRedeModelo", "2011", StatusBicicleta.REPARO_SOLICITADO);

        bicicletaRetirarDaRede.setTranca("trancaRetirarDaRede");
        this.bicicletas.add(bicicleta1);
        this.bicicletas.add(bicicleta2);
        this.bicicletas.add(bicicleta3);
        this.bicicletas.add(bicicletaTeste);
        this.bicicletas.add(bicicletaRetirarDaRede);
        this.bicicletas.add(bicicletaDelete);
    }

    public ArrayList<Bicicleta> getAll(){
 
        return this.bicicletas;
    }

    public Bicicleta getById(String id) throws NaoEncontradoException {
        Bicicleta bicicletaRecuperada = null;
        for(Bicicleta bicicleta : this.bicicletas){
            if(bicicleta.getId().equals(id)){
                bicicletaRecuperada = bicicleta;
                break;
            }
        }

        if(bicicletaRecuperada != null){
            return bicicletaRecuperada;
        }
        else{
            throw new NaoEncontradoException(this.mensagemBicicletaNaoEncontrada);
        }

    }

    public Bicicleta add(Bicicleta bicicleta) throws DadosInvalidosException{
        if(verificarCamposObrigatorios(bicicleta)){
            Bicicleta novaBicicleta = new Bicicleta(bicicleta.getModelo(), 
                                                    bicicleta.getMarca(), 
                                                    bicicleta.getAno(),
                                                    StatusBicicleta.NOVA);
            this.bicicletas.add(novaBicicleta);
            return novaBicicleta;
        }
        else{
            throw new DadosInvalidosException(this.mensagemDadosInvalidos);
        }
    }

    public Bicicleta update(String id, Bicicleta bicicleta) throws DadosInvalidosException, NaoEncontradoException{
        try{
            Bicicleta bicicletaRecuperada = this.getById(id);
            if(verificarCamposObrigatorios(bicicleta)){
                int index = this.retornaIndexBicicletaNaLista(id);
                bicicletaRecuperada.setModelo(bicicleta.getModelo());
                bicicletaRecuperada.setMarca(bicicleta.getMarca());
                bicicletaRecuperada.setAno(bicicleta.getAno());
                if(!bicicleta.getTranca().isEmpty()){
                    bicicletaRecuperada.setTranca(bicicleta.getTranca());
                }
                this.bicicletas.set(index, bicicletaRecuperada);
                return bicicletaRecuperada;
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemBicicletaNaoEncontrada);
        }
    }

    public void delete(String id) throws NaoEncontradoException, DadosInvalidosException{
        try{
            Bicicleta bicicletaRecuperada = this.getById(id);
            if(this.validarExclusaoBicicleta(bicicletaRecuperada)){
                this.bicicletas.remove(bicicletaRecuperada);
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
            
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemBicicletaNaoEncontrada);
        }
    }

    public Bicicleta changeStatus(String id, String acao) throws DadosInvalidosException, NaoEncontradoException{
        try{
            Bicicleta bicicletaRecuperada = this.getById(id);
            int index = this.retornaIndexBicicletaNaLista(id);
            switch (acao) {
                case "NOVA":
                    bicicletaRecuperada.setStatus(StatusBicicleta.NOVA);
                    break;
                case "DISPONIVEL":
                    bicicletaRecuperada.setStatus(StatusBicicleta.DISPONIVEL);
                    break;
                case "EM_USO":
                    bicicletaRecuperada.setStatus(StatusBicicleta.EM_USO);
                    break;
                case "REPARO_SOLICITADO":
                    bicicletaRecuperada.setStatus(StatusBicicleta.REPARO_SOLICITADO);
                    break;
                case "APOSENTADA":
                    bicicletaRecuperada.setStatus(StatusBicicleta.APOSENTADA);
                    break;
                case "EM_REPARO":
                    bicicletaRecuperada.setStatus(StatusBicicleta.EM_REPARO);
                    break;       
                default:
                    throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
            this.bicicletas.set(index, bicicletaRecuperada);
            return bicicletaRecuperada;
                

        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemBicicletaNaoEncontrada);
        }
    }

    public void inserirNaRede(BicicletaNaRede bicicletaNaRede, TrancaService trancaService) throws NaoEncontradoException, DadosInvalidosException, StatusInvalidoException{
        try{
            Bicicleta bicicletaRecuperada = this.getById(bicicletaNaRede.getIdBicicleta());
            Tranca trancaRecuperada = trancaService.getById(bicicletaNaRede.getIdTranca());
            if(this.validarInserirBicicletaNaRede(bicicletaRecuperada, trancaRecuperada)){
                bicicletaRecuperada.setTranca(bicicletaNaRede.getIdTranca());
                trancaRecuperada.setBicicleta(bicicletaRecuperada);
                trancaService.changeStatus(trancaRecuperada.getId(), "OCUPADA");
                this.changeStatus(bicicletaRecuperada.getId(), "DISPONIVEL");
                trancaService.update(trancaRecuperada.getId(), trancaRecuperada);
                this.update(bicicletaRecuperada.getId(), bicicletaRecuperada);
            }
            else if(this.validarInserirBicicletaNaRedeEmUso(bicicletaRecuperada)) {
            	throw new StatusInvalidoException("Bicicleta em uso");
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemBicicletaNaoEncontrada);
        }
    }

    public void retirarDaRede(BicicletaNaRede bicicletaNaRede, TrancaService trancaService) throws NaoEncontradoException, DadosInvalidosException{
        try{
            Bicicleta bicicletaRecuperada = this.getById(bicicletaNaRede.getIdBicicleta());
            Tranca trancaRecuperada = trancaService.getById(bicicletaNaRede.getIdTranca());
            if(this.validarRetirarBicicletaDaRede(bicicletaRecuperada, trancaRecuperada)){
                bicicletaRecuperada.setTranca("");
                trancaRecuperada.setBicicleta(null);
                trancaService.changeStatus(trancaRecuperada.getId(), "LIVRE");
                if(bicicletaNaRede.getStatus().equals(APOSENTADA))
                    this.changeStatus(bicicletaRecuperada.getId(), APOSENTADA);
                else if(bicicletaNaRede.getStatus().equals(EM_REPARO))    
                    this.changeStatus(bicicletaRecuperada.getId(), EM_REPARO);
                else
                    throw new DadosInvalidosException(this.mensagemDadosInvalidos);   
                     
                trancaService.update(trancaRecuperada.getId(), trancaRecuperada);
                this.update(bicicletaRecuperada.getId(), bicicletaRecuperada);
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemBicicletaNaoEncontrada);
        }
    }

    private boolean verificarCamposObrigatorios(Bicicleta bicicleta){
        if(!bicicleta.getModelo().isEmpty() && !bicicleta.getMarca().isEmpty() && !bicicleta.getAno().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }

    private int retornaIndexBicicletaNaLista(String id){
        int indexBicicleta = -1;
        for(Bicicleta bicicleta : this.bicicletas){
            indexBicicleta++;
            if(bicicleta.getId().equals(id)){
                break;
            }
        }
        return indexBicicleta;
    }

    private boolean validarExclusaoBicicleta(Bicicleta bicicleta){
        if(bicicleta.getStatus().equals(StatusBicicleta.APOSENTADA) && bicicleta.getTranca().equals("")){
            return true;
        }
        else{
            return false;
        }
    }

    private boolean validarInserirBicicletaNaRede(Bicicleta bicicleta, Tranca tranca){
        if((bicicleta.getStatus().equals(StatusBicicleta.NOVA) || bicicleta.getStatus().equals(StatusBicicleta.EM_REPARO)) &&  tranca.getStatus().equals(StatusTranca.LIVRE)){
            return true;
        }
        else{
            return false;
        }
    }
    
    private boolean validarInserirBicicletaNaRedeEmUso(Bicicleta bicicleta){
        if((bicicleta.getStatus().equals(StatusBicicleta.EM_USO))){
            return true;
        }
        else{
            return false;
        }
    }

    private boolean validarRetirarBicicletaDaRede(Bicicleta bicicleta, Tranca tranca){
        if(!(bicicleta.getStatus().equals(StatusBicicleta.DISPONIVEL) || tranca.getStatus().equals(StatusTranca.LIVRE)) &&  bicicleta.getStatus().equals(StatusBicicleta.REPARO_SOLICITADO)){
            return true;
        }
        else{
            return false;
        }
    }
}