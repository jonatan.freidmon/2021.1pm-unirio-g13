package echo.services;

import java.util.ArrayList;
import echo.exceptions.DadosInvalidosException;
import echo.exceptions.NaoEncontradoException;
import echo.models.Tranca;
import echo.models.Totem;
import echo.models.Bicicleta;
import echo.models.StatusTranca;

public class TotemService {

    final String mensagemDadosInvalidos = "Dados invalidos.";
    final String mensagemTotemNaoEncontrada = "Totem nao foi encontrado.";
    public ArrayList<Totem> totens;

    public TotemService(){
        this.totens = new ArrayList<Totem>();
        Totem totem = new Totem("dsakdmaskdas-dasdmaskdma-d1928191", "Rua Teste, Ramos, RJ");
        Totem totemDelete = new Totem("totemDelete", "Rua Teste, Ramos, RJ");
        Totem totem2 = new Totem("teste", "Rua cabo reis, Ramos, RJ");
        Totem totem3 = new Totem("totemRetirarDaRede", "Rua Uranos, Ramos, RJ");
        Tranca retirarDaRede = new Tranca("retirarDaRede", "Piscinao, ramos, rj", 
                "tranca 21590", 
                "2021",
                StatusTranca.EM_REPARO);
        Tranca tranca1 = new Tranca("5ea95fe2-18d9-4e0c-a073-08cb30e3e4e4", "Rua Teste, rj", 
                "tranca 2159", 
                "2018",
                StatusTranca.LIVRE);
        totem3.addTranca(retirarDaRede);
        totem3.addTranca(tranca1);
        totens.add(totem2);
        totens.add(totem3);
        totens.add(totem);
        totens.add(totemDelete);
        
    }

    public ArrayList<Totem> getAll(){
 
        return this.totens;
    }

    public Totem getById(String id) throws NaoEncontradoException {
        Totem totemRecuperada = null;
        for(Totem totem : this.totens){
            if(totem.getId().equals(id)){
                totemRecuperada = totem;
                break;
            }
        }

        if(totemRecuperada != null){
            return totemRecuperada;
        }
        else{
            throw new NaoEncontradoException(this.mensagemTotemNaoEncontrada);
        }

    }

    public Totem add(Totem totem) throws DadosInvalidosException{
        if(verificarCamposObrigatorios(totem)){
            Totem novoTotem = new Totem(totem.getLocalizacao());
            this.totens.add(novoTotem);
            return novoTotem;
        }
        else{
            throw new DadosInvalidosException(this.mensagemDadosInvalidos);
        }
    }

    public Totem update(String id, Totem totem) throws DadosInvalidosException, NaoEncontradoException{
        try{
            Totem totemRecuperada = this.getById(id);
            if(verificarCamposObrigatorios(totem)){
                int index = this.retornaIndexTotemNaLista(id);
                totemRecuperada.setLocalizacao(totem.getLocalizacao());
                totemRecuperada.setTrancas(totem.getTrancas());
                this.totens.set(index, totemRecuperada);
                return totemRecuperada;
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTotemNaoEncontrada);
        }
    }

    public void delete(String id) throws NaoEncontradoException, DadosInvalidosException{
        try{
            Totem totemRecuperada = this.getById(id);
            if(this.validarExclusaoTotem(totemRecuperada)){
                this.totens.remove(totemRecuperada);
            }
            else{
                throw new DadosInvalidosException(this.mensagemDadosInvalidos);
            }
            
        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTotemNaoEncontrada);
        }
    }

    public ArrayList<Bicicleta> getBicicletasByTotem(String id) throws NaoEncontradoException{
        try{
            ArrayList<Bicicleta> bicicletas = new ArrayList<Bicicleta>();
            Totem totemRecuperada = this.getById(id);
            for(Tranca tranca: totemRecuperada.getTrancas()){
                if(tranca.getBicicleta() != null){
                    bicicletas.add(tranca.getBicicleta());
                }
            }

            return bicicletas;

        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTotemNaoEncontrada);
        }

    }

    public ArrayList<Tranca> getTrancasByTotem(String id) throws NaoEncontradoException{
        try{
            Totem totemRecuperada = this.getById(id);
            return totemRecuperada.getTrancas();

        }
        catch(NaoEncontradoException ex){
            throw new NaoEncontradoException(this.mensagemTotemNaoEncontrada);
        }

    }
    
    private boolean verificarCamposObrigatorios(Totem totem){
        if(!totem.getLocalizacao().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    
    private int retornaIndexTotemNaLista(String id){
        int indexTotem = -1;
        for(Totem totem : this.totens){
            indexTotem++;
            if(totem.getId().equals(id)){
                break;
            }
        }
        return indexTotem;
    }

    private boolean validarExclusaoTotem(Totem totem){
        if(totem.getTrancas().size() == 0){
            return true;
        }
        else{
            return false;
        }
    }
}