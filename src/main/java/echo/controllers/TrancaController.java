package echo.controllers;
import echo.services.TrancaService;
import echo.services.TotemService;
import echo.services.BicicletaService;
import io.javalin.http.Context;

import java.time.LocalDate;

import echo.apis.ApiAluguel;
import echo.apis.ApiExterno;
import echo.exceptions.*;
import echo.models.Tranca;
import echo.models.Bicicleta;
import echo.models.Funcionario;
import echo.models.TrancaNaRede;
public class TrancaController {
    
    private TotemService totemService;
    private TrancaService trancaService;
    final String mensagem422 = "{'codigo':'422','message':'Dados invalidos.'}";
    final String mensagem404 = "{'codigo':'404','message':'Tranca nao encontrada.'}";
    final ApiExterno apiExterno = new ApiExterno();
    final ApiAluguel apiAluguel = new ApiAluguel();
    private Funcionario funcionario;
    
    public TrancaController(TotemService totemService, TrancaService trancaService){
        this.totemService = totemService;
        this.trancaService = trancaService;
        this.funcionario = this.apiAluguel.CreateFuncionario();
    }

    public void getAll(Context ctx) {
        ctx.json(this.trancaService.getAll());
        ctx.status(200);
    }

    public void getById(String id, Context ctx) {
        try{
            ctx.json(this.trancaService.getById(id));
            ctx.status(200);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }

    public void add(Context ctx) {
        try{
            Tranca tranca = ctx.bodyAsClass(Tranca.class);
            ctx.json(this.trancaService.add(tranca));
            ctx.status(200);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }

    public void update(String id, Context ctx) {
        try{
            Tranca tranca = ctx.bodyAsClass(Tranca.class);
            ctx.json(this.trancaService.update(id, tranca));
            ctx.status(200);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }
    
    public void delete(String id, Context ctx) {
        try{
            this.trancaService.delete(id);
            ctx.status(200);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }
    
    public void changeStatus(String id, String acao, Context ctx) {
        try{
            ctx.json(this.trancaService.changeStatus(id, acao));
            ctx.status(200);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }

    public void getBicicletaByTranca(String id, Context ctx){
        try{
            Bicicleta bicicleta = this.trancaService.getBicicletaByTranca(id);
            if(bicicleta == null){
                ctx.json("{'codigo':'404','message':'Bicicleta nao encontrada.'}");
                ctx.status(404);
            }
            else{
                ctx.json(bicicleta);
                ctx.status(200);
            }
            
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }

    public void inserirNaRede(Context ctx) {
        try{
            TrancaNaRede trancaNaRede = ctx.bodyAsClass(TrancaNaRede.class);
            this.trancaService.inserirNaRede(trancaNaRede, this.totemService);
            ctx.status(200);
            String message = "Funcionario: " + this.funcionario.getNome() + " - Numero da Tranca: " + trancaNaRede.getIdTranca() + " - Numero do totem: " + trancaNaRede.getIdTotem() + " - Data da inserção: " + LocalDate.now() ;
            ctx.json(message);
            this.apiExterno.SendEmail(this.funcionario.getEmail(), message);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
        catch(Exception ex) {
        	ctx.status(200);
        	ctx.json("Email nao foi enviado");
        }
    }

    public void retirarDaRede(Context ctx) {
        try{
            TrancaNaRede trancaNaRede = ctx.bodyAsClass(TrancaNaRede.class);
            this.trancaService.retirarDaRede(trancaNaRede, this.totemService);
            ctx.status(200);
            String message = "Funcionario: " + this.funcionario.getNome() + " - Numero da Tranca: " + trancaNaRede.getIdTranca() + " - Numero do totem: " + trancaNaRede.getIdTotem() + " - Data remoção: " + LocalDate.now() ;
            ctx.json(message);
            this.apiExterno.SendEmail(this.funcionario.getEmail(), message);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
        catch(Exception ex) {
        	ctx.status(200);
        	ctx.json("Email nao foi enviado");
        }
    }
}
