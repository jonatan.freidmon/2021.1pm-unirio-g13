package echo.controllers;
import echo.services.TotemService;
import io.javalin.http.Context;
import echo.exceptions.*;
import echo.models.Tranca;
import echo.models.Bicicleta;
import echo.models.Totem;
import java.util.ArrayList;
public class TotemController {
    
    private TotemService totemService;
    final String mensagem422 = "{'codigo':'422','message':'Dados invalidos.'}";
    final String mensagem404 = "{'codigo':'404','message':'Totem nao encontrada.'}";

    public TotemController(TotemService totemService){
        this.totemService = totemService;
    }

    public void getAll(Context ctx) {
        ctx.json(this.totemService.getAll());
        ctx.status(200);
    }

    public void add(Context ctx) {
        try{
            Totem totem = ctx.bodyAsClass(Totem.class);
            ctx.json(this.totemService.add(totem));
            ctx.status(200);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }

    public void update(String id, Context ctx) {
        try{
            Totem totem = ctx.bodyAsClass(Totem.class);
            ctx.json(this.totemService.update(id, totem));
            ctx.status(200);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }
    
    public void delete(String id, Context ctx) {
        try{
            this.totemService.delete(id);
            ctx.status(200);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }
    

    public void getBicicletasByTotem(String id, Context ctx){
        try{
            ArrayList<Bicicleta> bicicletas = this.totemService.getBicicletasByTotem(id);
            if(bicicletas == null){
                ctx.json("{'codigo':'404','message':'Bicicletas nao encontradas.'}");
                ctx.status(404);
            }
            else{
                ctx.json(bicicletas);
                ctx.status(200);
            }
            
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }

    public void getTrancasByTotem(String id, Context ctx){
        try{
            ArrayList<Tranca> trancas = this.totemService.getTrancasByTotem(id);
            if(trancas == null){
                ctx.json("{'codigo':'404','message':'Trancas nao encontradas.'}");
                ctx.status(404);
            }
            else{
                ctx.json(trancas);
                ctx.status(200);
            }
            
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }
}
