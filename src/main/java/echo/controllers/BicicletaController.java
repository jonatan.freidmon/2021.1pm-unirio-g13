package echo.controllers;
import echo.services.TrancaService;
import echo.services.BicicletaService;
import io.javalin.http.Context;

import java.io.IOException;
import java.time.LocalDate;

import echo.apis.ApiAluguel;
import echo.apis.ApiExterno;
import echo.exceptions.*;
import echo.models.Bicicleta;
import echo.models.BicicletaNaRede;
import echo.models.Funcionario;

public class BicicletaController {
    
    final String mensagem422 = "{'codigo':'422','message':'Dados invalidos.'}";
    final String mensagem404 = "{'codigo':'404','message':'Bicicleta nao encontrada.'}";
    final String mensagemErroEmail = "Nao foi possivel enviar email";
    final ApiExterno apiExterno = new ApiExterno();
    final ApiAluguel apiAluguel = new ApiAluguel();
    private TrancaService trancaService;
    private BicicletaService bicicletaService;
    private Funcionario funcionario;

    public BicicletaController(TrancaService trancaService, BicicletaService bicicletaService){
        this.trancaService = trancaService;
        this.bicicletaService = bicicletaService;
        this.funcionario = this.apiAluguel.CreateFuncionario();	
    }

    public void getAll(Context ctx) {
        ctx.json(this.bicicletaService.getAll());
        ctx.status(200);
    }

    public void getById(String id, Context ctx) {
        try{
            ctx.json(this.bicicletaService.getById(id));
            ctx.status(200);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }

    public void add(Context ctx) {
        try{
            Bicicleta bicicleta = ctx.bodyAsClass(Bicicleta.class);
            ctx.json(this.bicicletaService.add(bicicleta));
            ctx.status(200);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }

    public void update(String id, Context ctx) {
        try{
            Bicicleta bicicleta = ctx.bodyAsClass(Bicicleta.class);
            ctx.json(this.bicicletaService.update(id, bicicleta));
            ctx.status(200);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
    }
    
    public void delete(String id, Context ctx) {
        try{
            this.bicicletaService.delete(id);
            ctx.status(200);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }
    
    public void changeStatus(String id, String acao, Context ctx) {
        try{
            ctx.json(this.bicicletaService.changeStatus(id, acao));
            ctx.status(200);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
    }

    public void inserirNaRede(Context ctx) {

        BicicletaNaRede bicicletaNaRede = ctx.bodyAsClass(BicicletaNaRede.class);
        try{
            this.bicicletaService.inserirNaRede(bicicletaNaRede, this.trancaService);
            ctx.status(200);
            String message = "Funcionario: " + this.funcionario.getNome() + " - Numero da bicicleta: " + bicicletaNaRede.getIdBicicleta() + " - Numero da tranca: " + bicicletaNaRede.getIdTranca() + " - Data da inserção: " + LocalDate.now() ;
            ctx.json(message);
            this.apiExterno.SendEmail(this.funcionario.getEmail(), message);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }
        catch(StatusInvalidoException ex) {
        	ctx.status(422);
        	ctx.json("Devolucao solicitada");
        	this.apiAluguel.DevolvaBicicleta(bicicletaNaRede.getIdTranca(),bicicletaNaRede.getIdBicicleta());
        }
        catch(Exception ex){
            ctx.json(this.mensagemErroEmail);
            ctx.status(200);
        }
    }

    public void retirarDaRede(Context ctx) {
        try{
            BicicletaNaRede bicicletaNaRede = ctx.bodyAsClass(BicicletaNaRede.class);
            this.bicicletaService.retirarDaRede(bicicletaNaRede, this.trancaService);
            ctx.status(200);
            String message = "Funcionario: " + this.funcionario.getNome() + " - Numero da bicicleta: " + bicicletaNaRede.getIdBicicleta() + " - Numero da tranca: " + bicicletaNaRede.getIdTranca() + " - Data da remoção: " + LocalDate.now() ;
            ctx.json(message);
            this.apiExterno.SendEmail(this.funcionario.getEmail(), message);
        }
        catch(NaoEncontradoException ex){
            ctx.json(this.mensagem404);
            ctx.status(404);
        }
        catch(DadosInvalidosException ex){
            ctx.json(this.mensagem422);
            ctx.status(422);
        }

        catch(Exception ex){
            ctx.json(this.mensagemErroEmail);
            ctx.status(200);
        }
    }
}
