package echo.apis;

import kong.unirest.Unirest;


public class ApiExterno {
	
	private String url = "https://uniriobike.herokuapp.com/";

    public void SendEmail(String email, String message){
    	String jsonInputString = "{\"email\": \"" + email + "\", \"mensagem\": \"" + message + "\"}";
    	Unirest.post(url + "/enviarEmail").body(jsonInputString).asEmpty();
    }
	
}
