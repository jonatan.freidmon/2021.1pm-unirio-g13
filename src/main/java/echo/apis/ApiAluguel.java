package echo.apis;

import org.codehaus.jackson.map.ObjectMapper;

import kong.unirest.Unirest;


import echo.models.Funcionario;

public class ApiAluguel {
	
	private String url = "https://trabalho-pm-henrique.herokuapp.com/";

    public Funcionario CreateFuncionario() {

    	String jsonInputString = "{\"senha\": \"jonatan123@\", \"email\": \"jonatansouza99@gmail.com\", \"nome\":\"Jonatan Freidmon\", \"idade\":21,\"funcao\":\"Reparador\", \"cpf\":\"12345678912\"}";
    	Funcionario funcionario = null;
    	
    	try {
    	    funcionario = Unirest.post(url + "/funcionario").body(jsonInputString).asObject(Funcionario.class).getBody();
    	}
    	catch (Exception ex) {
    	    return null;
    	}
    	
    	return funcionario;
    }
    
    public void DevolvaBicicleta(String idTranca, String idBicicleta) {
    	String jsonInputString = "{\"idTranca\": \"" + idTranca + "\", \"idBicicleta\":\"" + idBicicleta +"\" }";
    	Unirest.post(url + "/devolucao").body(jsonInputString).asEmpty();
    }
	
}
