package echo.util;

import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;
import echo.Controller;
import java.lang.String;
import echo.controllers.BicicletaController;
import echo.controllers.TrancaController;
import echo.controllers.TotemController;
import echo.services.BicicletaService;
import echo.services.TrancaService;
import echo.services.TotemService;

public class JavalinApp {

    private BicicletaService bicicletaService = new BicicletaService();
    private TrancaService trancaService = new TrancaService();
    private TotemService totemService = new TotemService();
    private BicicletaController bicicletaController = new BicicletaController(this.trancaService, this.bicicletaService);
    private TrancaController trancaController = new TrancaController(this.totemService, this.trancaService);
    private TotemController totemController = new TotemController(this.totemService);

    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                    path("/", () -> get(Controller::getRoot));
                    path("/bicicleta", ()-> {

                        get(ctx -> {
                            bicicletaController.getAll(ctx);
                        });

                        get(":id", ctx -> {
                            bicicletaController.getById(String.valueOf(ctx.pathParam("id")), ctx);
                        });

                        post(ctx -> {
                            bicicletaController.add(ctx);
                        });

                        put(":id", ctx -> {
                            bicicletaController.update(String.valueOf(ctx.pathParam("id")),ctx);
                        });
                        
                        delete(":id", ctx -> {
                            bicicletaController.delete(String.valueOf(ctx.pathParam("id")),ctx);
                        });

                        post(":id/status/:acao", ctx -> {
                            bicicletaController.changeStatus(String.valueOf(ctx.pathParam("id")), String.valueOf(ctx.pathParam("acao")), ctx);
                        });

                        post("/integrarNaRede", ctx -> {
                            bicicletaController.inserirNaRede(ctx);
                        });
                        post("/retirarDaRede", ctx -> {
                            bicicletaController.retirarDaRede(ctx);
                        });
                    });
                    path("/tranca", ()-> {

                        get(ctx -> {
                            trancaController.getAll(ctx);
                        });

                        get(":id", ctx -> {
                            trancaController.getById(String.valueOf(ctx.pathParam("id")), ctx);
                        });

                        post(ctx -> {
                            trancaController.add(ctx);
                        });

                        put(":id", ctx -> {
                            trancaController.update(String.valueOf(ctx.pathParam("id")),ctx);
                        });
                        
                        delete(":id", ctx -> {
                            trancaController.delete(String.valueOf(ctx.pathParam("id")),ctx);
                        });

                        post(":id/status/:acao", ctx -> {
                            trancaController.changeStatus(String.valueOf(ctx.pathParam("id")), String.valueOf(ctx.pathParam("acao")), ctx);
                        });
                        get(":id/bicicleta", ctx -> {
                            trancaController.getBicicletaByTranca(String.valueOf(ctx.pathParam("id")), ctx);
                        });
                        post("/integrarNaRede", ctx -> {
                            trancaController.inserirNaRede(ctx);
                        });
                        post("/retirarDaRede", ctx -> {
                            trancaController.retirarDaRede(ctx);
                        });
                    });
                    path("/totem", ()-> {

                        get(ctx -> {
                            totemController.getAll(ctx);
                        });

                        post(ctx -> {
                            totemController.add(ctx);
                        });

                        put(":id", ctx -> {
                            totemController.update(String.valueOf(ctx.pathParam("id")),ctx);
                        });
                        
                        delete(":id", ctx -> {
                            totemController.delete(String.valueOf(ctx.pathParam("id")),ctx);
                        });

                        get(":id/bicicletas", ctx -> {
                            totemController.getBicicletasByTotem(String.valueOf(ctx.pathParam("id")), ctx);
                        });

                        get(":id/trancas", ctx -> {
                            totemController.getTrancasByTotem(String.valueOf(ctx.pathParam("id")), ctx);
                        });
                    });
                });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
